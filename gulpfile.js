var gulp = require('gulp'),
    pug  = require('gulp-pug'),
    scss = require('gulp-sass'),
    browserSync = require('browser-sync'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    minifyhtml = require('gulp-minify-html'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    jsmin = require('gulp-jsmin'),
    autoprefixer = require('gulp-autoprefixer'),
    postcss = require('gulp-postcss'),
    watch = require('gulp-watch'),
    runSequence = require('run-sequence'),
    rimraf = require('rimraf'),
    plumber = require('gulp-plumber');




gulp.task('pug', function(){
        return gulp.src('app/index.pug')
        .pipe(plumber())
        .pipe(pug())
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream());
});


gulp.task('imagemin', function(){
    return gulp.src('app/img/**/*')
    .pipe(imagemin({
        interlaced: true,
        progressive: true,
        use: [pngquant()]
    }))
    .pipe(gulp.dest('dist/img'))
});

gulp.task('style', () => {
  const processors = [
    require('autoprefixer'),
    require('postcss-fixes'),
    // require('doiuse')({
    // browsers: [
    //   '> 1%',
    //   'last 15 Chrome versions',
    //   'last 15 Opera versions',
    //   'IE >= 11',
    //   'last 17 Firefox versions',
    //   'Safari >= 9',
    //   'Android >= 4',
    //   'UCAndroid >= 10',
    //   'iOS >= 8'
    // ],
    // ignore: ['outline']
    // }),
    require('postcss-sorting')
  ];
  return gulp.src('app/css/**/*.scss')
    .pipe(scss().on('error', scss.logError))
    .pipe(postcss(processors) )
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream());
});

gulp.task('build', function(){
    var  bildFonts = gulp.src('app/fonts/**/*')
           .pipe(gulp.dest('dist/fonts'));
    var  bildJs = gulp.src('app/js/**/*')
          .pipe(gulp.dest('dist/js'));
});

gulp.task('watch', () => {
  watch(['app/index.pug'], () => {
    gulp.start('pug');
    gulp.watch('dist/index.html', browserSync.reload);
  });
  watch(['app/css/style.scss'], () => {
    gulp.start('style');
    gulp.watch('dist/css/style.css', browserSync.reload);
  });
});


gulp.task('browser-sync', function(){
    browserSync({
      server: {
        baseDir: 'dist/'
      },
      open: false,
      port: 9000,
      reloadDelay: 300
    });
});

gulp.task('clean', (cb) => {
  rimraf('./dist/', cb);
});

gulp.task('cssnano', function(){
    return gulp.src('dist/css/**/*')
    .pipe(cssnano())
    .pipe(gulp.dest('dist/css'))
});

gulp.task('jsmin', function(){
    return gulp.src('dist/js/**/*')
    .pipe(jsmin())
    .pipe(gulp.dest('dist/js'))
});


gulp.task('default', () => {
  runSequence('clean', 'pug', 'style', 'imagemin', 'build', 'browser-sync', 'watch');
});

gulp.task('dist', () => {
  runSequence('clean', 'cssnano', 'jsmin');
});